# https://www.plenom.com/download/70652/
# Busylight-for-MS-Teams-Release-x.y.z.zip
#
@{
    url = 'https://www.plenom.com/download/70652/Busylight-for-MS-Teams-Release-2.4.10.zip'
    checksum = 'e84171a67c5ca3d2762bbcd4915f00b3d95880be82087cd208c537f4a8232b65'
    checksumType = 'sha256'
}

$ErrorActionPreference = 'Stop';

# $packageName = $env:ChocolateyPackageName
# $version = '2.4.0'
# $basePackageName = 'Busylight-for-MS-Teams-Release-' + $version
# $fullPackage = $basePackageName + '.zip'
# $WorkSpace = Join-Path $env:TEMP "$packageName.$env:chocolateyPackageVersion"
$data = & (Join-Path -Path (Split-Path -Path $MyInvocation.MyCommand.Path) -ChildPath data.ps1)
$toolsDir = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

# $WebFileArgs = @{
#     packageName         = $packageName
#     FileFullPath        = Join-Path $WorkSpace $fullPackage
#     GetOriginalFileName = $true

#     Url64bit            = $url64
#     Checksum64          = $checkSum64
#     ChecksumType        = 'sha256'
# }

# $PackedInstaller = Get-ChocolateyWebFile @WebFileArgs

# $UnzipArgs = @{
#     PackageName  = $packageName
#     FileFullPath = $PackedInstaller
#     Destination  = $WorkSpace
# }

# Get-ChocolateyUnzip @UnzipArgs


$packageArgs = @{
    packageName    = $env:ChocolateyPackageName
    unzipLocation  = $toolsDir
    fileType       = 'msi'
    softwareName   = '*Busylight*Teams*'
    silentArgs     = "/qn /norestart /l*v `"$($env:TEMP)\$($packageName).$($env:chocolateyPackageVersion).MsiInstall.log`""
    validExitCodes = @(0)

    url             = $data.url
    checksum        = $data.checksum
    checksumType    = $data.checksumType
}

Install-ChocolateyZipPackage @packageArgs
